/** 
 \file tnt-cmd.h
 \author Giandomenico Rossi <g.rossi@tattile.com>
 \copyright 2020 Tattile s.r.l.
 

 \brief
*/


#ifndef _TNT_CMD_H
#define _TNT_CMD_H

enum tnt_source {
    OCR    = 0,      // 0b00,
    CTX    = 1,      // 0b01,
    DOUBLE = 2,      // 0b10.
    
    N_DEV,    
};

enum tnt_gmode {
    TRIGGED     = 0, // 0b000,
    CONTINUOUS  = 4, // 0b100,
};

enum tnt_grabmode {
    TRIGGED_OCR    =  TRIGGED | OCR,
    TRIGGED_CTX    =  TRIGGED | CTX,
    TRIGGED_DOUBLE =  TRIGGED | DOUBLE,
    
    CONTINUOUS_OCR     = CONTINUOUS | OCR,
    CONTINUOUS_CTX     = CONTINUOUS | CTX,
    CONTINUOUS_DOUBLE  = CONTINUOUS | DOUBLE,

    TNT_N_MODES
};

#define FG_SOURCE(s)   ((s) & 0b011)
#define FG_GMODE(m)    ((m) & 0b100)
#define FG_MODE(s, m)  (FG_SOURCE(s) | FG_GMODE(m))

enum tnt_sensors {
	CMOS_IMX265_BW  = 162,
	CMOS_IMX265_COL = 163,
	CMOS_IMX250_BW  = 164,
	CMOS_IMX250_COL = 165,
	CMOS_IMX250_BW_POLARIZED_CROPPED_3MP = 166,
	CMOS_IMX250_COL_POLARIZED_CROPPED_3MP = 167,
};

struct tnt_cmd {
    int channel;
    int value;
};

#define V4L2_CID_OPTICAL_DENSITY_BASE      (V4L2_CID_USER_MEYE_BASE)
#define V4L2_CID_OPTICAL_DENSITY_Y         (V4L2_CID_USER_MEYE_BASE+1)
#define V4L2_CID_OPTICAL_DENSITY_CB        (V4L2_CID_USER_MEYE_BASE+2)
#define V4L2_CID_OPTICAL_DENSITY_CR        (V4L2_CID_USER_MEYE_BASE+3)
#define V4L2_CID_OPTICAL_DENSITY_START_X   (V4L2_CID_USER_MEYE_BASE+4)
#define V4L2_CID_OPTICAL_DENSITY_END_X     (V4L2_CID_USER_MEYE_BASE+5)
#define V4L2_CID_OPTICAL_DENSITY_START_Y   (V4L2_CID_USER_MEYE_BASE+6)
#define V4L2_CID_OPTICAL_DENSITY_END_Y     (V4L2_CID_USER_MEYE_BASE+7)

struct tnt_od_ch {
    uint32_t  y;
    uint32_t cb;
    uint32_t cr;    
};

struct tnt_optd {
    uint32_t idx;
    struct tnt_od_ch ocr;
    struct tnt_od_ch ctx;
};

enum tnt_processing {
    TNT_OPTICAL_DENSITY, //!< Optical density
    TNT_RLE              //!< Rle
};

#define RLE_ALGO_NUM 2
#define TNT_RLE_LUT_LEN 256

struct tnt_rle_conf
{
    uint32_t max_hor_size_for_reload_start;     // Distanza dopo la quale la coordinata di start stroke si resetta
    uint32_t max_hor_size_char;                 // Larghezza massima stroke
    uint32_t min_hor_size_char;                 // Larghezza minima stroke
    uint32_t num_differential_pair;             // Numero di transizioni consecutive da verificare (1-3)
    uint32_t pixel_distance;                    // Distanza pixel transizione(il differenziale � fatto tra il pixel attuale e quello a pixel_distance)
    uint32_t num_byte_max;                      // Dimensione massima RLE (multiplo di 512 byte)
    uint8_t lut[TNT_RLE_LUT_LEN];
    uint32_t size_y;
    uint32_t size_x;
    uint32_t black_plates_enable;
};

enum search_type {
    EXACT= 0,
    NEAREST
};



#define MAX_ACQ_POINTS	(6)

typedef uint32_t u32;
typedef uint64_t u64;

struct acq_point {
    u32 shutter;
	u32 gain;
	u32 strobe;
    u32 r_gain;
	u32 g_gain;
	u32 b_gain;
};

struct acq_points {
	u32 n;
	struct acq_point pts[MAX_ACQ_POINTS];
};

struct tnt_metadata{
    struct acq_point acq;
    struct tnt_od_ch od;
    u64 uid;
    u64 timestamp;
    u32 i_acq;
    u32 index;
    u32 ch;
};

#define TNT_MAGIC 'T'
#define TNT_GRAB_FRAME     _IO (TNT_MAGIC,  1)
#define TNT_SET_GRABMODE   _IOW(TNT_MAGIC,  2, int)
#define TNT_SET_OUTPUT     _IOW(TNT_MAGIC,  3, int)
#define TNT_SET_PERIOD     _IOW(TNT_MAGIC,  4, int)
#define TNT_INIT_SENSOR    _IOWR(TNT_MAGIC, 5, struct tnt_sensor)

#define TNT_GET_OPTD       _IOWR(TNT_MAGIC, 6, struct tnt_optd)
#define TNT_ENABLE_PROCS   _IOW(TNT_MAGIC,  7, int)
#define TNT_FINALIZE	   _IOW(TNT_MAGIC,  8, int)
#define TNT_SET_ACQUISITION_POINTS _IOW(TNT_MAGIC, 9, struct acq_points)

#define TNT_QBUF           _IOWR(TNT_MAGIC, 10, struct tnt_frames)
#define TNT_DQBUF          _IOWR(TNT_MAGIC, 11, struct tnt_frames)
#define TNT_SET_RLE_ALGO_A _IOW(TNT_MAGIC, 12, struct tnt_rle_conf)
#define TNT_SET_RLE_ALGO_B _IOW(TNT_MAGIC, 13, struct tnt_rle_conf)
#define TNT_GET_METADATA   _IOWR(TNT_MAGIC, 14, struct tnt_metadata)
#define TNT_DQBUF_UID      _IOWR(TNT_MAGIC, 15, struct v4l2_buffer)



#endif
