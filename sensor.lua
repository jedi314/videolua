local ffi = require "ffi"
local S = require "syscall"



ffi.cdef [[
enum tcd_sensor_code_t {
    ONSEMI_PYTHON5000_BW = 142,     //!< ON Semiconductor Python5000 monochrome
    ONSEMI_PYTHON5000_COL = 143,    //!< ON Semiconductor Python5000 bayer color

    ONSEMI_PYTHON2000_BW = 148,     //!< ON Semiconductor Python2000 monochrome
    ONSEMI_PYTHON2000_COL = 149,    //!< ON Semiconductor Python2000 bayer color

    SONY_IMX249_COL = 157,          //!< Sony IMX249 bayer color

    SONY_IMX265_BW  = 162,           //!< Sony IMX265 monochrome
    SONY_IMX265_COL = 163,           //!< Sony IMX265 bayer color
    SONY_IMX250_BW  = 164,           //!< Sony IMX250 monochrome
    SONY_IMX250_COL = 165,           //!< Sony IMX250 bayer color
	    
	SONY_IMX250_BW_POLARIZED_CROPPED_3MP = 166,           //!< Sony IMX250 monochrome polarized cropped 2048X1536
	SONY_IMX250_COL_POLARIZED_CROPPED_3MP = 167,          //!< Sony IMX250 bayer color cropped 2048X1536
    SONY_IMX265_BW_CROPPED_2MP = 168,   //!< Sony IMX265 monochrome cropped 1920x1080
    SONY_IMX265_COL_CROPPED_2MP = 169,      
}; 

struct tnt_sensor {
    enum tnt_sensors type;      /*!< Sensor code to be set */
    char  format_name[16];      /*!< Sensor video format name  */
    uint32_t pixel_type;        /*!< Sensor video format code  */
    uint32_t width;             /*!< Video format  width */
    uint32_t height;            /*!< Video format  height  */
    uint32_t bpp;               /*!< Video bit per pixel  */
    uint32_t channel_id;        /*!< channel sensor is connected to  */
};



]]

local TNT_INIT_SENSOR  = S.c.IOCTL._IOWR('T', 5, ffi.sizeof("struct tnt_sensor"))
local TNT_RLE_EN  = S.c.IOCTL._IOW('T',   5, "int")
local Sensor = {}

function Sensor:open()
   local sd = assert(S.open(self.fname, "rdwr"))
   return sd
end

function Sensor:new(sname, scode, devname)
   local s = {name=sname, code = scode, fname=devname}
   setmetatable(s, {__index = self})
   return s
end

function Sensor:close(fd)
   S.close(fd)
end

function Sensor:init()
   local sensor = ffi.new("struct tnt_sensor")
   sensor.type = self.code
   local fd = self:open()
   local ok, err = fd:ioctl(TNT_INIT_SENSOR, sensor)
   if err then
      io.write("Error initialize sensor: ")
      print(err)
   end
   fd:close()
   return ok, err
end



function Sensor:rle(algo, conf)
   local rle = ffi.new("struct tnt_rle")
   if conf ~= nil then
      ffi.copy(rle.conf, conf, ffi.sizeof("struct tnt_rle_conf"))
   end
   local fd = self:open()
   local ok,err =  fd:ioctl(TNT_INIT_, self.code)
   fd:close()
end


return Sensor
