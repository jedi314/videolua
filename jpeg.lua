local ffi = require "ffi"
local S = require "syscall"
local c = S.c
local types = S.types
local t, pt, s = types.t, types.pt, types.s
local lfs = require "lfs"

ffi.cdef[[
/* jpeg */

typedef uint32_t __u32;
typedef uint64_t u64;
typedef int32_t __s32;
typedef uint8_t  __u8;


enum jp_type {
    RGB   = 0,
    YCbCr = 1
};


enum jp_subsample {
    SAMPLE_4_4_4 = 0,
    SAMPLE_4_2_2 = 1,           /* horizontal subsamnpling */
    SAMPLE_4_2_0 = 3,           /* sub sampling */
};


enum colorspace {
    COLOR = 0,
    GRAY = 1,
};

enum input_format {
    GRAY8 = 0,
    YCrCb420 = 1,
    NV12 = 2,
};

enum output_format {
    BPP8 = 0,
    BPP24 = 1,
};


enum address_type {
    PHYSICAL_ADDRESS = 0,
    VIRTUAL_ADDRESS = 1,
};


struct jp_buffer {
    uint64_t addr;
    uint32_t size;
};

struct jp_config {
    enum jp_type      type;
    enum jp_subsample subsample;
    uint32_t           gray;
    uint32_t           width;
    uint32_t           height;
    uint32_t           quality;
    struct jp_buffer   in;
    struct jp_buffer   out;
} ;


struct tjpeg_core_info {
    int bbp;                    /**!< bit per pixel of the core  */
    int rev;                    /**!< core revision  */
    int max_pixel_width;        /**!< Maximum pixel width  */
    int in_axi4st;
    int out_axi4st;
    int address_width;
};


struct jp_image {
    enum colorspace cs;
    uint64_t in_buf;
    uint64_t out_buf;
    uint32_t size;
    uint32_t  width;
    uint32_t  height;
};



struct tjpeg_image_t {
    enum address_type  addr_type;
    void*              input_buffer_addr;
    uint32_t           input_buffer_size; /**!< Size 4 KB units of the coded output buffer. */
    void*              out_buffer_addr;
    uint32_t           out_buffer_size;   /**!< Size 4 KB units of the coded output buffer. */
    uint32_t           offset;            /**!< Tattile metadata reserved  memory */
    enum input_format  in_fmt;
    enum output_format out_fmt;
    uint32_t  width;
    uint32_t  height;
};

]]


ffi.cdef [[
void dump(char *desc, void *addr, int len);
int tjpeg_convert(struct tjpeg_image_t *image);
void *malloc(uint64_t);
void free(void *);
int tjpeg_open();
int tjpeg_configure(struct jp_config *conf);
int tjpeg_close();
inline uint32_t appn_size(uint8_t* jpeg, uint32_t off);

]]




local cc = ffi.C


-- jpepg
local TJ_IN_BUFFER  =   c.IOCTL._IOW('J',  1, ffi.sizeof("uint64_t"))
local TJ_START      =   c.IOCTL._IOR('J',  3, ffi.sizeof("uint32_t"))
local TJ_CONFIGURE  =   c.IOCTL._IOWR('J', 4, ffi.sizeof("struct jp_config" ))
local TJ_GET_INFO   =   c.IOCTL._IOR('J',  5, ffi.sizeof("struct tjpeg_core_info"))
local TJ_CONVERT    =   c.IOCTL._IOR('J',  6, ffi.sizeof("struct jp_image"))



--- JPEG Class ---
JP = { dev = "/dev/tjpeg0" }
function JP:new(jtype, gray)
   local j={}
   local config = ffi.new("struct jp_config")
   config.type = jtype or cc.YCbCr
   config.gray = gray or 1
   config.quality = 70
   config.subsample = cc.SAMPLE_4_2_0
   j.config = config
   setmetatable(j, {__index = self})
   return j
end

function JP:open()
   local fd, err = S.open(self.dev, "rdwr")
   if err then
      assert(err, "Error Open file " .. self.dev)
      return nil
   end
   self.fd = fd
end

function JP:close()
   self.fd:close()
end


function JP:info()
   local fd = self.fd
   local info = ffi.new("struct tjpeg_core_info")
   local ok, err = fd:ioctl(TJ_GET_INFO, info)
   if err ~= nil then
      print("get_info error: " .. tostring(err))
      return nil
   end
   return info
end

function JP:img_size(w,h)
   self.config.width = w
   self.config.height = h
end

function JP:configure()
   local fd = self.fd
   local ok, err = fd:ioctl(TJ_CONFIGURE, self.config)
   if err then
      print("JP configure error: " .. tostring(err))
   end
end


function JP:gray()
   self.config.gray = 1
end


function JP:color()
   self.config.gray = 0
end

function JP:convert(image)
   local fd = self.fd
   local img = ffi.new("struct jp_image")
   img.in_buf = image.in_addr
   local ok, err = fd:ioctl(TJ_CONVERT, img)
   if err ~= nil then
      print(tostring(err))
      return nil
   end

   print(tostring(img.out_buf))
   image.out_addr = img.out_buf
   print(tostring(image.out_addr))

   image.size = img.size
   return image
end

function JP:map(image)
   local fd = self.fd
   print(tostring(image.size))
   local addr, err = fd:mmap(nil, image.size, "read, write", "shared", image.out_addr)
   if err ~= nil then
      print("Error map: " .. tostring(err))
      return nil
   end
   return addr
end

function JP:dump(image, size)
   local addr = self:map(image)
   if addr == nil then
      return
   end
   jpg.dump(nil, addr, size or image.size)
   self:unmap(image)
end

function JP:unmap(image)
   local fd = self.fd
   S.munmap(image.out_addr, image.size)
end

function JP:serialize(image)
   local fd = self.fd
   S.munmap(image.out_addr, image.size)
end



Image = {in_addr = 0, out_addr = 0, size = 0, offset = 4096 }
function Image:new(addr)
   local i={}
   i.in_addr = addr
   setmetatable(i, {__index = self})
   return i
end

-----------------------------------------------------------------

-- function jpeg_convertion(jpeg, conf, name, img_size)
--    local ret = jpg.tjpeg_configure(conf)


-- end

function split(s, sep)
    local fields = {}

    local sep = sep or " "
    local pattern = string.format("([^%s]+)", sep)
    string.gsub(s, pattern, function(c) fields[#fields + 1] = c end)

    return fields
end


function read_image(fname, size)
   local buf_size = size or 9*1024*1024
   local fd, err = S.open(fname, "rdonly")
   if err then
      print(err)
   end

   local p = ffi.gc(ffi.C.malloc(buf_size), ffi.C.free)
   local rd = assert(fd:read(p, buf_size))
   fd:close()
   return p, rd
end

function write_image(name, img)
   local fd, err = S.open(name, "wronly, creat", "0644")
   if err then
      print("Open ouptut: " .. tostring(err))
      return err
   end
   local n, errw = fd:write(img.out_buffer_addr, img.out_buffer_size)
   if(errw) then
      print(errw)
   else
      print("Write " .. name.. " " .. tostring(n) .. " bytes" )
   end
   fd:close()
end

function list_files(path, fnames)
   local files = {}
   for i,f in ipairs(fnames)  do
      local file = split(f, "-.")
      local image = ffi.new("struct tjpeg_image_t")
      image.addr_type = cc.VIRTUAL_ADDRESS
      image.offset = 0
      image.width = tonumber(file[2])
      image.height = tonumber(file[3])
      image.in_fmt = cc.GRAY8
      image.out_fmt = cc.BPP8
      image.input_buffer_addr, image.input_buffer_size = read_image(path..f, image.width*image.height)
      files[f]=image
      print("name: " .. f .. " - " .. " " .. files[f].width .. " " .. files[f].height)
   end
   return files
end



function create_files(jpg, path, n, d )
   local files = {}
   for i=0,n do
      local color = math.random(0, 255)
      local rows  = math.random(16,  192*16)
      local cols  = math.ceil(math.random(16,  192*16)/16) * 16 -- width must be multiple of 16 pixels
      --local cols  = math.random(16,  192*16) -- width must be multiple of 16 pixels

      local buf_size = math.min(9*1024*1024 ,rows*cols)
      local name = "f_" .. "_" .. tostring((i+d*100)) .. "-" .. tostring(cols) .. "-" .. tostring(rows) .. ".raw"
      local aname = path .. name
      print(name)
      local image = ffi.new("struct tjpeg_image_t")
      print(tostring(i) .. " - " .. tostring(d))
      image.addr_type = cc.VIRTUAL_ADDRESS
      image.offset = 0
      image.width = cols
      image.height = rows
      image.in_fmt = cc.GRAY8
      image.out_fmt = cc.BPP8
      -- image.input_buffer_addr, image.input_buffer_size = read_image("gray8.raw", buf_size)

      local p = ffi.gc(ffi.C.malloc(buf_size), ffi.C.free)
      ffi.fill(p, buf_size, color)
      image.input_buffer_addr = p
      image.input_buffer_size = buf_size

      -- local fd, err = S.open(aname, "wronly, creat", "0644")
      -- local n, errw = fd:write(p, ssize)
      -- print("Write " .. tostring(n))
      -- if(errw) then
      --    print(errw)
      -- else
      --    print("Create " .. aname)
      -- end
      -- fd:close()
      files[i] = image
   end
   return files
end

function jpeg_conf(jpg)
   local conf = ffi.new("struct jp_config")
   local ret = jpg.tjpeg_open()
   io.write("Open Device: ")
   if ret == -1 then
      print("KO")
      os.exit()
   else
      print("OK")
   end
   math.randomseed(os.clock()*100000000000)

   print("Configure gray")
   conf.type = cc.YCbCr
   conf.gray = 1
   conf.quality = 90
   conf.subsample = cc.SAMPLE_4_2_0
   conf.width  = 2048
   conf.height = 1536

   ret = jpg.tjpeg_configure(conf)
   print("Conf: " .. tostring(ret))
end

function tcd_test()
   local jpg = ffi.load("/usr/lib/libtjpeg.so")

   local ipath = "/root/in/"
   local opath = "/root/out/"

   os.execute("rm -f /root/out/*")
   os.execute("rm -f /root/in/*")
   print("LibTJPEG unit Test")

   jpeg_conf(jpg)
   for j=0,0 do
      local files = create_files(jpg, ipath, 100, j)

      -- GRAY8 conf
      print("Open IMGs")
      --local ft = list_files(ipath, files)

      for i=0,#files  do
         print("Convert Image# " .. tostring(i))
         ret = jpg.tjpeg_convert(files[i])
         if ret ~= 0 then
            print("JPEG ERROR: " .. tostring(ret))
            return
         end

         --jpg.dump(nil, image.out_buffer_addr, 500)

         local name = opath .. "out" .. tostring(i+j*100) .. ".jpeg"
         write_image(name, files[i])
         files[i].input_buffer_addr = nil
         files[i] = nil
      end
      files=nil
      collectgarbage("collect")
   end
   jpg.tjpeg_close()
   os.execute("scp /root/out/* jedi@172.25.99.1:~/tmp")

end


function tcd_test1()
   local jpg = ffi.load("/usr/lib/libtjpeg.so")

   print("LibTJPEG unit Test")
   jpeg_conf(jpg)

   local bufs={}

   local image = ffi.new("struct tjpeg_image_t")
   image.addr_type = cc.VIRTUAL_ADDRESS
   image.offset = 0
   image.width = 2048
   image.height = 1536
   image.in_fmt = cc.GRAY8
   image.out_fmt = cc.BPP8
   image.input_buffer_addr, image.input_buffer_size = read_image("gray8.raw", buf_size)
   print(image.input_buffer_addr)
   print("GRAY: " .. tostring(rd))

   local image1 = ffi.new("struct tjpeg_image_t")
   image1.addr_type = cc.VIRTUAL_ADDRESS
   image1.offset = 0
   image1.width = 960
   image1.height = 540
   image1.in_fmt = cc.GRAY8
   image1.out_fmt = cc.BPP8
   image1.input_buffer_addr, image1.input_buffer_size = read_image("gray8950.raw")
   print("GRAY1: " .. tostring(rd))

   bufs[0] = image
   bufs[1] = image1
   --bufs[2] = image1
   --bufs[3] = image



   --jpg.dump(nil, bufs[0].input_buffer_addr, 16)

   for i=0,#bufs do
      print("Image# " .. tostring(i))

      local img = bufs[i]
      local addr = img.input_buffer_addr
      jpg.dump(nil, addr, 16)
      print("width: " .. tostring(img.width) .. " height: " .. tostring(img.height))
      ret = jpg.tjpeg_convert(img)
      print("Convert: " .. tostring(ret))
      if ret ~= 0 then
         print("Bye")
         return
      end

      --jpg.dump(nil, image.out_buffer_addr, 500)
      local name = "out" .. tostring(i) .. ".jpeg"

      write_image(name, img)

   end
   jpg.tjpeg_close()

end






return { jp= JP, test=tcd_test }
