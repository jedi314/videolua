local ffi = require "ffi"
local S = require "syscall"
--local reflect = require "reflect"



ffi.cdef[[
/* tnt */

typedef uint32_t __u32;
typedef uint64_t u64;
typedef int32_t __s32;
typedef uint8_t  __u8;

struct v4l2_timecode {
	__u32	type;
	__u32	flags;
	__u8	frames;
	__u8	seconds;
	__u8	minutes;
	__u8	hours;
	__u8	userbits[4];
};

struct v4l2_plane {
	__u32			bytesused;
	__u32			length;
	union {
		__u32		mem_offset;
		unsigned long	userptr;
		__s32		fd;
	} m;
	__u32			data_offset;
	__u32			reserved[11];
};

struct v4l2_buffer {
	__u32			index;
	__u32			type;
	__u32			bytesused;
	__u32			flags;
	__u32			field;
	struct timeval		timestamp;
	struct v4l2_timecode	timecode;
	__u32			sequence;
	__u32			memory;
	union {
		__u32           offset;
		unsigned long   userptr;
		struct v4l2_plane *planes;
		__s32		fd;
	} m;
	__u32			length;
	__u32			reserved2;
	union {
		__s32		request_fd;
		__u32		reserved;
	};
};


struct v4l2_control {
	__u32		     id;
	__s32		     value;
};

enum tnt_ctrl {
    V4L2_CID_RED_BALANCE    = 0x98090e, 
    V4L2_CID_GREEN_BALANCE  = 0x980924,
    V4L2_CID_BLUE_BALANCE   = 0x98090f,
    V4L2_CID_GAIN           = 0x980913,
    V4L2_CID_EXPOSURE       = 0x980911,
};


struct processor_layout {
    char name[16];
    char devname[32];
    int  plane_id;
    uint32_t w;
    uint32_t h;
    uint32_t size;
    
    int index;
    int status;
};

struct nplanes {
       int n;
};

]]

local cc = ffi.C

local a = S.c.IOCTL._IOWR('T', 6, ffi.sizeof("struct tnt_optd"))
local TNT_GRAB_FRAME     = S.c.IOCTL._IO ('T',   1)
local TNT_SET_OUTPUT     = S.c.IOCTL._IOW('T',   3, "int")
local TNT_GET_OPTD       = S.c.IOCTL._IOWR('T',  6, ffi.sizeof("struct tnt_optd"))
--local TNT_ENABLE_RLE     = S.c.IOCTL._IOW('T',   7, ffi.sizeof("struct tnt_rle"))
local TNT_FINALIZE       = S.c.IOCTL._IOW('T',   8, "int")
local TNT_SET_ACQ_PTS    = S.c.IOCTL._IOW('T',   9, ffi.sizeof("struct acq_points"))


local TNT_SET_RLE_ALGO_A  = S.c.IOCTL._IOW('T',  12, ffi.sizeof("struct tnt_rle_conf"))
local TNT_SET_RLE_ALGO_B  = S.c.IOCTL._IOW('T',  13, ffi.sizeof("struct tnt_rle_conf"))
local TNT_ENUM_PROCESSOR  = S.c.IOCTL._IOWR('T', 17, ffi.sizeof("struct processor_layout"))
local TNT_PROCESSOR_INFO  = S.c.IOCTL._IOWR('T', 22, ffi.sizeof("struct processor_layout"))
local TNT_NPLANES         = S.c.IOCTL._IOR('T', 28, ffi.sizeof("struct nplanes"))

Channel = { ocr = 0, rlea = 1, ctx = 2, rleb = 3 } -- meta
Plane = { name=0, devname=0, id = 0, status=0 , w = 0, h =0 , size=0, off = 0, addr=0 }
function Plane:new(ch, pl)
   local p = {}
   setmetatable(p, {__index = self})
   p.name = ffi.string(pl.name)
   p.devname = ffi.string(pl.devname)
   p.id = pl.plane_id
   p.status = pl.status
   p.channel = ch
   p.w = pl.w
   p.h = pl.h
   p.size = pl.size
   return p
end

function Plane:map(frame)
   local ch = self.channel
   ch:map(frame, self)
end

function Plane:print()
   print("Name: " .. self.name)
   print("Device: " .. self.devname)
   print("Plane: " .. self.id)
   print("W: " .. self.w)
   print("H: " .. self.h)
   print("Size: " .. self.size)
end

function Channel:open()
   local fd, err = S.open(self.name, "rdwr")
   if err then
      print("OPEN ERROR " .. self.name)
      print(err)
      return nil
   end
   self.fd = fd
   return fd
end

function Channel:new(name, sensor)
   local d = {
      name = name,
      sensor = sensor or 0,
      rle = 0,
      fd = nil,
      n_planes = 0,
      planes = {}
   }
   setmetatable(d, {__index = self})
   d:open()
   if sensor then
      local ok, err = d:attach(sensor)
      if err  then
         return nil, err
      end
   end
   d.n_planes = d:nplanes()
   return d, nil
end

function Channel:nplanes()
   local fd = self.fd
   local nplanes = ffi.new("struct nplanes")

   local ret, err = fd:ioctl(TNT_NPLANES, nplanes)
   if err then  print(err); return end
   return nplanes.n
end


function Channel:close()
   self.fd:close()
end

function Channel:enable_rle(algo, conf)
   self.rle = true
   self.sensor:rle(algo, conf)
end

function Channel:disable_rle()
   self.rle = 0
   self.sensor:rle(cc.DISABLE_RLE)
end


function Channel:attach(sensor)
   self.sensor = sensor
   local ok, err
   if type(self.sensor) == "table" then
      ok, err = sensor:init()
      if err then
         io.write("Channel:attach: ")
         print(err)
      end
   end
   return ok, err
end

function Channel:ctrl(ctrl, value)
   local fd = self.fd
   local c = ffi.new("struct v4l2_control")
   c.id = name
   c.value = value
   local ok, err = fd:ioctl(VIDIOC_S_CTRL, ctrl)
   if err then print(err) end
end


function Channel:gain(value)
   self:ctrl(V4L2_CID_GAIN, value)
end

function Channel:red(value)
   self:ctrl(V4L2_CID_RED_BALANCE, value)
end

function Channel:green(value)
   self:ctrl(V4L2_CID_GREEN_BALANCE, value)
end

function Channel:blue(value)
   self:ctrl(V4L2_CID_BLUE_BALANCE, value)
end

function Channel:expo(value)
   self:ctrl(V4L2_CID_EXPOSURE, value)
end


function Channel:map(frame, plane)
   local fd = self.fd
   local addr, err = fd:mmap(nil, plane.size, "read, write", "shared", frame.planes[plane.id].offset)
   if err then
      io.write("mmap error: ")
      print(err)
      return
   end
   frame.planes[sel.name].vaddr = addr
end

function Channel:unmap(frame)
   local fd = self.fd
   local ok, err = fd:munmap(frame.planes[self.name].lenght)
   if err then print("munmap error: " .. err); return end
end

function Channel:enum_component()
   local fd = self.fd
   local planes = {}
   local npl = -1

   repeat
      local pl = ffi.new("struct processor_layout")
      pl.index = npl
      local ok, err = fd:ioctl(TNT_ENUM_PROCESSOR, pl)
      npl = npl+1
      self.planes[ffi.string(pl.name)] = Plane:new(self, pl)
   until not ok
end


function Channel:component_info(comp)
   local pl = ffi.new("struct processor_layout")
   local fd = self.fd
   
   ffi.copy(pl.name, "pippo")
   local ok, err = fd:ioctl(TNT_PROCESSOR_INFO, pl)
   if err then print(err); return end
   print("pl.name: " .. ffi.string(pl.name))
   print("pl.plane: " .. pl.plane_id)
end

function Channel:get_plane(name)
   local plane = self.planes[name]
   return plane
end

return Channel


