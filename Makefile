SCP = scp
END=
LIBS= libsend_image.so  \
	libsend_image_wrapper.so \
	$(END)

SRC= axle.lua		\
	 channel.lua	\
	 frame.lua		\
	 grabber.lua	\
	 jpeg.lua		\
	 processors.lua	\
	 sendimage.lua	\
	 sensor.lua		\
	 strobe.lua		\
	 t.lua			\
	 lcpp.lua		\
	 syscall.lua	\
	 tnt-cmd.h		\
	 $(END)

IHOST?=getafix


.PHONY: install 


install: $(SRC) $(LIBS)
	@scp $? root@$(IHOST):~


# .FORCE:

# %.lua: .FORCE
# 	$(SCP) $@ root@$(IHOST):~
