local ffi = require "ffi"
local S = require "syscall"
local c = S.c
local cc = ffi.C

ffi.cdef[[

struct processor_layout {
    char name[32];
    int  plane_id;
    int index;
};


]]
local TNT_ENUM_PROCESSOR = S.c.IOCTL._IOWR('T', 17, ffi.sizeof("struct processor_layout"))




