local ffi = require "ffi"
local S = require "syscall"

ffi.cdef[[
/* sendimage */

int create_test_receive_socket();
int close_socket(int s);
int send_image(int s, char *ip, uint16_t port, unsigned long width, unsigned long height, unsigned long size, long type, long pixel_type, void *buf);

]]


Sender={}
function Sender:new(addr, w, h, px)
   local s = { fd= 0, addr=0, bank = 0, port=1002 }
   s.size  = (w or 2048) * (h or 1536)
   s.pixel = px or 8
   setmetatable(s, {__index = self})
   return s
end

function Sender:ip2int(str)
   local num = 0
   if str and type(str)=="string" then
      local o1,o2,o3,o4 = str:match("(%d+)%.(%d+)%.(%d+)%.(%d+)" )
      num = 2^24*o1 + 2^16*o2 + 2^8*o3 + o4
   end
   return num
end


function Sender:send(f)
   for k,p in pairs(f,planes) do
      self.fd = si.send_image(self.fd, self.addr, self.port, self.w, self.h, self.size, 0, 6, p.offset);
   end
end


function Sender:open()
   self.fd = si.create_test_receive();
   print(self.fd)
end


function Sender:close()
   local ok = si.close_socket(self.fd)
end


return Sender
