local ffi = require "ffi"
local S = require "syscall"
local Sensor = require "sensor"
local cc = ffi.C

ffi.cdef[[
/* axl */


struct delta {
    u32 ymin;
    u32 ymax;
};


struct lut_info{
    uint32_t w;                 /**< width of lut table   */
    uint32_t h;                 /**< hight of lut table   */ 
    uint64_t phy;               /**< lut physical address */
    uint32_t size;              /**< size of lut table    */
    uint32_t max_displ;
    uint32_t delta_max;
};

struct lut_table {
    char *name;
    int fd;
    uint32_t* addr;
    struct lut_info info;
};


int create_lut(struct lut_table *lt, uint16_t *lutx, uint16_t *luty);
int axl_create_lut(uint16_t *lutx, uint16_t *luty);
void dump(char *desc, void *addr, int len);
int axl_init(uint16_t *lutx, uint16_t *luty);

]]


--local TNT_ENUM_PROCESSOR      = S.c.IOCTL._IOWR('T', 17, ffi.sizeof("struct processor_layout"))
local REMAPPER_REQUEST_LUT    = S.c.IOCTL._IOWR('T', 18, ffi.sizeof("struct lut_info"))
local REMAPPER_NEW_DELTA_COEF = S.c.IOCTL._IOW('T',  19, ffi.sizeof("struct delta"))
local REMAPPER_SET_DELTA_COEF = S.c.IOCTL._IOW('T',  20, ffi.sizeof("uint64_t"))
local REMAPPER_INIT           = S.c.IOCTL._IO('T', 21)


local tcd = ffi.load("/usr/lib/libtcd.so")

Remapper={ devname="/dev/remapper" }
function Remapper:new()
   local r={}
   r.w = 0
   r.h = 0
   r.lutfd = 0  
   --local fd = assert(S.open(self.devname, "rdwr"))
   --r.fd = fd
   --local ok, err = fd:ioctl(REMAPPER_INIT)
   --if err then print(err); return end
   setmetatable(r, {__index = self})
   return r
end


function Remapper:lut_info()
   local fd = assert(S.open(self.devname, "rdwr"))
   local lut_info = ffi.new("struct lut_info")
   lut_info.h = 991
   local ok, err = fd:ioctl(REMAPPER_REQUEST_LUT, lut_info)
   if err then print(err); return end
   fd:close()
   self.lut = lut_info
end

function Remapper:print_lut_info()
   if self.lut == nil then
      print("Lut empty")
      return
   end
   local lut = self.lut
   print("----------------------")
   print("w: " .. tonumber(lut.w))
   print("h: " .. tonumber(lut.h))
   print("size: " .. tonumber(lut.size))
   print("max displ: " .. tonumber(lut.max_displ))
   print("delta max: " .. tonumber(lut.delta_max))
   print("----------------------")
end

function Remapper:map_lut()
   local fd = assert(S.open("/proc/lut", "rdwr"))
   local addr, err = fd:mmap(nil, self.lut.size, "read, write", "shared", self.lut.phy)
   if err then print(err); end
   self.lut_addr = addr
   self.lutfd = fd
end

function Remapper:unmap_lut()
   local ok, err = S.munmap(self.lut_addr, self.lut.size)
   if err then print(err); end
   self.lutfd:close()
end

function Remapper:create_lut(lutx, luty)
   local lt = ffi.new("struct lut_table")
   lt.fd = self.lutfd:getfd()
   lt.addr = self.lut_addr
   lt.info = self.lut
   local ret = tcd.create_lut(lt, ffi.cast('unsigned short*', lutx),
                              ffi.cast('unsigned short*', luty))
   return ret
end

function Remapper:close()
   self.fd:close()
end


Axl={}
function Axl:new()
   local a={}
   a.grabber = Grabber:new()
   a.remapper = Remapper:new()
   setmetatable(a, {__index = self})
   return a
end


function Axl:load_lut(path)
   local file = io.open(path, "rb") -- r read mode and b binary mode
   if not file then
      print("Open file error: " .. path);
      return nil
   end
   local content = file:read("*a") -- *a or *all reads the whole file
   file:close()
   local t={}
   content:gsub('%-?%d+', function(n) t[#t+1] = tonumber(n); end)
   tt=ffi.new("unsigned short int[?]", #t, t)
   
   return tt   
end


function Axl:read_lut(path)
   local buf={}
   local file = assert(io.open(path, "rb") )-- r read mode and b binary mode
   local content = file:read("*all") -- *a or *all reads the whole file
   file:close()

   content:gsub('%-?%d+', function(n) buf[#buf+1] = tonumber(n); end)
   print(path .. " size: " .. tostring(#buf))
   local tt=ffi.new("uint16_t[?]", #buf, buf)
   return tt
end


function Axl:load_lut(path)
   local lut = self:read_lut(path)
   if lut == nil then
      return 
   end
   return lut
end


function Axl:close()
   self.fd:close()
end

function Axl:create_lut()
   if self.lutx == nil or self.luty == nil then
      print("lut table not loaded" )
      return
   end
   local ret = tcd.axl_init(ffi.cast('unsigned short*', self.lutx),
                             ffi.cast('unsigned short*', self.luty))
   return ret
end


function Axl:mode(mode, sens)
   self.grabber:mode(mode, "cha", sens)
end

function test(lx, ly)
   local lutx = lx or "lut.x"
   local luty = ly or "lut.y"
   local axl = Axl:new()
   axl.remapper:lut_info()
   axl.remapper:print_lut_info()
   axl.lutx = axl:load_lut(lutx)
   axl.luty = axl:load_lut(luty)
   local ret = axl:create_lut()
   print("init: " .. tonumber(ret))
   --axl.remapper:unmap_lut()
   
   -- local axl_sens = Sensor:new("imx265crop", cc.SONY_IMX265_BW_CROPPED_2MP, "/dev/sd-chb")
   -- axl.grabber:continuous_chb(axl_sens)
   -- axl.grabber:fps(45)
   -- axl.grabber:reqbufs(20)
   -- axl.grabber:queueall()
   -- axl.grabber:start()
end



return { Axl=Axl, test=test }
