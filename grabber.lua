local ffi = require "ffi"
local S = require "syscall"
local c = S.c
local cc = ffi.C
local Sensor = require "sensor"


ffi.cdef[[
/* tnt */

typedef uint32_t __u32;
typedef uint64_t u64;
typedef int32_t __s32;
typedef uint8_t  __u8;


struct v4l2_requestbuffers {
	__u32			count;
	__u32			type;		/* enum v4l2_buf_type */
	__u32			memory;		/* enum v4l2_memory */
	__u32			capabilities;
	__u32			reserved[1];
};

enum v4l2_buf_type {
	V4L2_BUF_TYPE_VIDEO_CAPTURE        = 1,
	V4L2_BUF_TYPE_VIDEO_OUTPUT         = 2,
	V4L2_BUF_TYPE_VIDEO_OVERLAY        = 3,
	V4L2_BUF_TYPE_VBI_CAPTURE          = 4,
	V4L2_BUF_TYPE_VBI_OUTPUT           = 5,
	V4L2_BUF_TYPE_SLICED_VBI_CAPTURE   = 6,
	V4L2_BUF_TYPE_SLICED_VBI_OUTPUT    = 7,
	V4L2_BUF_TYPE_VIDEO_OUTPUT_OVERLAY = 8,
	V4L2_BUF_TYPE_VIDEO_CAPTURE_MPLANE = 9,
	V4L2_BUF_TYPE_VIDEO_OUTPUT_MPLANE  = 10,
	V4L2_BUF_TYPE_SDR_CAPTURE          = 11,
	V4L2_BUF_TYPE_SDR_OUTPUT           = 12,
	V4L2_BUF_TYPE_META_CAPTURE         = 13,
	V4L2_BUF_TYPE_META_OUTPUT	       = 14,
	/* Deprecated, do not use */
	V4L2_BUF_TYPE_PRIVATE              = 0x80,
};

enum v4l2_memory {
	V4L2_MEMORY_MMAP             = 1,
	V4L2_MEMORY_USERPTR          = 2,
	V4L2_MEMORY_OVERLAY          = 3,
	V4L2_MEMORY_DMABUF           = 4,
};


struct tnt_frames {
    //enum search_type search;
    uint64_t timestamp;
    uint64_t uid;
    uint64_t n_future;
    int pre;                  /**!< # of frames before timestamp  */
    int post;                 /**!< # of frames after  timestamp  */
    int index[150];           /**!< array of a couple of id, value, value is search creiteria */
    int n;
};

]]



local TNT_SET_PERIOD   = S.c.IOCTL._IOW('T',   4, "int")
local TNT_SET_GRABMODE = S.c.IOCTL._IOW('T',   2, "int")
local VIDIOC_REQBUFS   = S.c.IOCTL._IOWR('V',  8, ffi.sizeof("struct v4l2_requestbuffers"))

local TNT_QBUF         = S.c.IOCTL._IOWR('T', 10, "int")
local TNT_DQBUF        = S.c.IOCTL._IOWR('T', 11, ffi.sizeof("struct tnt_frames"))
local TNT_DQBUF_UID    = S.c.IOCTL._IOWR('T', 15, ffi.sizeof("struct tnt_frames"))
local TNT_WAIT_NEXT    = S.c.IOCTL._IOWR('T', 16, ffi.sizeof("struct tnt_frames"))

-- v4l2
local VIDIOC_QUERYBUF  = S.c.IOCTL._IOWR('V',  9, ffi.sizeof("struct v4l2_buffer"))
local VIDIOC_QBUF      = S.c.IOCTL._IOWR('V', 15, ffi.sizeof("struct v4l2_buffer"))
local VIDIOC_DQBUF     = S.c.IOCTL._IOWR('V', 17, ffi.sizeof("struct v4l2_buffer"))
local VIDIOC_STREAMON  = S.c.IOCTL._IOW('V',  18, "int")
local VIDIOC_STREAMOFF = S.c.IOCTL._IOW('V',  19, "int")


Grabber = { mconf={trigged = 0, continuous=4},  sconf={ocr=0, ctx=1, double=2}, cha={}, chb={} }
function Grabber:new(alias)
   local g = { gmode = 0, source = 0, channels = {}, frames={}, alias={} }
   setmetatable(g, {__index = self})
   if alias ~= nil then
      g.alias = alias
   else
      print(g.alias)
   end
   return g
end


function Grabber:_mode(gmode, source, sensor0, sensor1)
   if type(gmode) == "string" then
      self.gmode= self.mconf[gmode]
      self.source = self.sconf[source]
      print("add_" .. gmode .. "_" .. source)
      self["add_" .. gmode .. "_" .. source](self, sensor0, sensor1)
   elseif type(gmode) == "number" and source == nil then
      self.source = bit.band(gmode, 3)
      self.gmode   = bit.band(gmode, 4)
   elseif type(gmode) == "number" and type(source) == "number" then
      self.source = source
      self.gmode  = gmode
   else
      return nil, "mode: format error"
   end
   return bit.bor(self.gmode, self.source), nil
end

function Grabber:revert_conf(c, conf)
   for k,m in pairs(conf) do
      if c == m then
         return k
      end
   end
   return ""
end

function Grabber:revert_mode()
   return self:revert_conf(self.gmode, self.mconf)
end

function Grabber:revert_source()
   return self:revert_conf(self.source, self.sconf)
end


function Grabber:mode(gmode, source, sensor0, sensor1)
   local ggmode,err = self:_mode(gmode, source, sensor0, sensor1)
   if err then print(err); return end
   local fd = assert(S.open("/dev/v4l-subdev0", "rdwr"))
   local ok, err = fd:ioctl(TNT_SET_GRABMODE, ggmode)
   local m = self:revert_mode()
   local s = self:revert_source()
   print("grabber: mode="..m .." source="..s)
   if err then print(err); return end
   fd:close()
end



function Grabber:append_ch(name, ch)
   self.channels[name] = ch
   self[name] = ch
end


function Grabber:dealias(name)
   if self.alias and self.alias[name] then
      return self.alias[name]
   end
   return name
end

function Grabber:add_ch(alias, sensor)
   local name = self:dealias(alias)
   local ch = "/dev/" .. name
   print(">>"..ch)
   local meta = getmetatable(self)

   local channel, err = Channel:new(ch, sensor or nil)
   if not channel then
      return nil, err
   end
   self:append_ch(name, channel)
   meta.__index[alias] = channel
   
   self.fd = self.fd or channel.fd
   return channel, nil
end

function Grabber:add_trigged_cha(sensor)
   return self:add_ch("cha", sensor or nil)
end


function Grabber:add_continuous_cha(sensor)
   return self:add_ch("cha", sensor or nil)
end


function Grabber:add_trigged_chb(sensor)
   return self:add_ch("chb",  sensor or nil)
end

function Grabber:add_continuous_chb(sensor)
   return self:add_ch("chb",  sensor or nil)
end

function Grabber:add_trigged_double(sensor0, sensor1)
   self:add_ch("cha", sensor0 or nil)
   self:add_ch("chb", sensor1 or nil)   
end

function Grabber:add_continuous_double(sensor0, sensor1)
   self:add_ch("cha", sensor0 or nil)
   self:add_ch("chb", sensor1 or nil)
end

function Grabber:trigged_cha(sensor)
   self:mode(cc.TRIGGED_OCR)
   self:add_trigged_cha(sensor)
end

function Grabber:trigged_chb(sensor)
   print("mode: " ..tostring(cc.TRIGGED_CTX))
   self:mode(cc.TRIGGED_CTX)
   return  self:add_trigged_chb(sensor)
end

function Grabber:trigged_double(sensor0, sensor1)
   self:mode(cc.TRIGGED_DOUBLE)
   self:add_trigged_double(sensor0, sensor1)
end

function Grabber:continuous_ocr(sensor)
   self:mode(cc.CONTINUOUS_OCR)
   self:add_continuous_ocr(sensor)
end


function Grabber:continuous_cha(sensor)
   self:mode(cc.CONTINUOUS_OCR)
   self:add_continuous_ocr(sensor)
end


function Grabber:continuous_ctx(sensor)
   self:mode(cc.CONTINUOUS_CTX)
   self:add_continuous_chb(sensor)
end


function Grabber:continuous_chb(sensor)
   self:mode(cc.CONTINUOUS_CTX)
   self:add_continuous_chb(sensor)
end


function Grabber:continuous_double(sensor0, sensor1)
   self:mode(cc.CONTINUOUS_DOUBLE)
   self:add_continuous_double(sensor0, sensor1)
end




function Grabber:reqbufs(n)
   self.nframes = n or 0
   local req = ffi.new("struct v4l2_requestbuffers")
   local fd = self.fd
   req.count = self.nframes
   req.type = cc.V4L2_BUF_TYPE_VIDEO_CAPTURE
   req.memory = cc.V4L2_MEMORY_MMAP
   local ret, err = fd:ioctl(VIDIOC_REQBUFS, req)
   if err then print("reqbuf error: " .. err); return end
   return ret, err
end

function Grabber:querybuf(id)
   if id >= self.nframes then
      return nil
   end
   if self.fd == nil then
      return nil
   end
   local fd = self.fd 
   local buf = ffi.new("struct v4l2_buffer")
   local planes = ffi.new("struct v4l2_plane [4]")

   buf.type = cc.V4L2_BUF_TYPE_VIDEO_CAPTURE
   buf.memory = cc.V4L2_MEMORY_MMAP
   buf.index = id
   buf.m.planes = planes
   local ret, err = fd:ioctl(VIDIOC_QUERYBUF, buf)
   if ret then
      local f = Frame:new(id)
      for k,ch in pairs(self.channels) do
         f.planes[k].offset = buf.m.planes[ch[k]].m.mem_offset
         f.planes[k].length = buf.m.planes[ch[k]].length
         self.frames[id] = f
      end
      return buf
   else
      print("querybuf error: " ..err)
   end
end

function Grabber:query_buffers()
   for i=1,self.nframes do
      local b = self:querybuf(i-1)
   end
end

function Grabber:close()
   for key,ch in pairs(self.channels) do
      ch:close()
   end
end



function Grabber:queue(id)
   local buf = ffi.new("struct v4l2_buffer")
   local planes = ffi.new("struct v4l2_plane[?]", 4)
   local fd = self.fd
   ffi.fill(buf, ffi.sizeof("struct v4l2_buffer"), 0)
   --ffi.fill(buf, ffi.sizeof("struct v4l2_plane[4]"), 0)
   
   buf.type = cc.V4L2_BUF_TYPE_VIDEO_CAPTURE
   buf.memory = cc.V4L2_MEMORY_MMAP
   buf.index = id
   buf.m.planes = planes

   local ret, err = fd:ioctl(VIDIOC_QBUF, buf)
   if err then print("Queue Error") end
end




function Grabber:queueall()
   for i=0,self.nframes-1 do
      self:queue(i)
   end
end


function Grabber:start()
   local buf = ffi.new("struct v4l2_buffer")
   local vtype = cc.V4L2_BUF_TYPE_VIDEO_CAPTURE
   local fd = self.fd
   local ok, err = fd:ioctl(VIDIOC_STREAMON, vtype)
   if err then
      print(err)
   else
      print("Grabe Started!")
   end
end


function Grabber:shot(n)
   local nshot = n or 1
   for i=0,nshot do
      print("Shot# "..tostring(i))
      self:start()
   end
end


function Grabber:stop()
end

function Grabber:dqueue(f)
   local fd = self.fd
   local buf = ffi.new("struct v4l2_buffer")
   local planes = ffi.new("struct v4l2_plane[?]", 8)
   buf.index = f or -1
   buf.type = cc.V4L2_BUF_TYPE_VIDEO_CAPTURE
   buf.memory = cc.V4L2_MEMORY_MMAP
   buf.m.planes = planes
   
   local ret, err = fd:ioctl(VIDIOC_DQBUF, buf)
   if err then print("DQueue Error") end

   return buf
end

function Grabber:dqpkt(timestamp, uid, pre, post)
   local frm = ffi.new("struct tnt_frames")

   frm.timestamp = timestamp
   frm.uid = uid
   frm.pre = pre
   frm.post = post

   return frm
end

function Grabber:dqnext(uid, pre)
   local fd = self.fd
   local frm = self:dqpkt(0, uid, pre, 0)
   local ret, err = fd:ioctl(TNT_WAIT_NEXT, frm)
   if err then
      io.write("dqnext: ")
      print(err)
   end
   
   return frm
end

function Grabber:nextfrm(frm)
   if frm.n == frm.pre + frm.post + 1 then
      return
   end
   local fnext = self:dqnext(frm.uid + frm.post, frm.n_future-1)
   for i=0,fnext.n-1 do
      frm.index[frm.n] = fnext.index[i]
      frm.n= frm.n + 1
   end
end

function Grabber:dequeue_by_ts(timestamp, pre, post)
   local fd = self.fd
   local frm = self:dqpkt(timestamp, 0, pre, post)

   local ret, err = fd:ioctl(TNT_DQBUF, frm)
   if err then
      io.write("DQueue Error: ")
      print(err)
   end
   self:nextfrm(frm)
   return frm
end


function Grabber:dequeue_by_uid(uid, pre, post)
   local fd = self.fd
   local frm = self:dqpkt(0, uid, pre, post)
   local ret, err = fd:ioctl(TNT_DQBUF_UID, frm)
   self:nextfrm(frm)
   return frm
end


function Grabber:next_frm()
   return self:dequeue_by_ts(0, 0, 0)
end


function Grabber:last_frm()
   return self:dequeue_by_ts(-1, 0, 0)
end

function Grabber:last_uid()
   return self:dequeue_by_uid(-1, 0, 0)
end


function Grabber:next_uid()
   return self:dequeue_by_uid(0, 0, 0)
end


function Grabber:period(us)
   local fd = assert(S.open("/dev/v4l-subdev0", "rdwr"))
   fd:ioctl(TNT_SET_PERIOD, us)
   fd:close()   
end
function Grabber:fps(f)
   local us = math.floor(1/f*1000000+0.5)
   self:period(us)
end

function Grabber:close()
   for k,v in pairs(self.channels) do
      v:close()
   end
end

function Grabber:attach(name, sensor)
   local channel = self[name]
   channel:attach(sensor)
end

function Grabber:mmap(frame)
   for k,d in pairs(channels) do
      d:map(frame)
   end
end

function Grabber:map_plane(frame, plane)

   
end


function Grabber:metadata(frame, plane)
   
end



Frame = {}
function Frame:new()
   local f={}
   setmetatable(f, {__index = self})
   return f
end



function test()
   local bw  = Sensor:new("imx265bw",  cc.CMOS_IMX265_BW, "/dev/v4l-subdev0")
   local col = Sensor:new("imx265col", cc.CMOS_IMX265_COL, "/dev/v4l-subdev1")
   local alias = { cha="video0", chb="video1" }
   local grabber = Grabber:new(alias)
   local init, err = grabber:continuous_chb(col)
   
   if err then
      print(err)
      os.exit()
   end

   grabber:fps(45)
   grabber:reqbufs(20)
   
   grabber:queueall()
   grabber:start()
   io.read()
   
   -- for i=0,10 do
   --    grabber:start()
   --    --io.read()
   --    local frm = grabber:dqueue(0)
   --    print("dequeued")
   --    --io.read()
   --    -- local pl = grabber.chb:get_plane("Video")
   --    -- local m = grabber:metadata(pl)
   --    grabber:queue(0)
   --    print("queued")
   --    --io.read()
   -- end
      
   
   -- local uid, pre, post = io.read("*n", "*n", "*n")
   -- local frm = grabber:dequeue_by_uid(uid, pre, post)
   -- local frm = grabber:next_frm()
   -- grabber.chb:enum_component()

   -- local pl = grabber.chb:get_plane("Video")
   -- pl:print()

   -- grabber:map_plane(frame, plane)
   -- local m = grabber:metadata(plane)
   -- m.print()
   local frm = grabber:next_frm()
   local ts = frm.timestamp
   print("ts: " .. tostring(ts))
   grabber:queue(frm.id)
   
   --local frmp = grabber.last(frm.index[0])
   --local frm1 = grabbero:last_frm()
   
   --grabber:last_uid()
   grabber:close()
   -- os.execute("read")
   -- local f = grabber:dqueue()
   -- print("Send image....")
   -- local s = Sender:new("172.25.100.68")
   -- grabber:close()
end


return {grabber=Grabber, test=test}

