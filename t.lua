local lcpp = require "lcpp"
local ffi = require "ffi"

-- local strict = require "test.strict"
local helpers = require "syscall.helpers"
local assert = helpers.assert
local S = require "syscall"
local abi = require "syscall.abi"

local abi = S.abi
local types = S.types
local t, pt, s = types.t, types.pt, types.s
local c = S.c
local util = S.util
local h = require "syscall.helpers"
local cc = ffi.C

-- load module
ffi.cdef ("#include \"tnt-cmd.h\"")
local Sensor = require "sensor"
local Channel = require "channel"

local JPG = require "jpeg"
local Jpeg = JPG.jp

local Grab = require "grabber"
local Grabber = Grab.Grabber

local Axle = require "axle"
local Axl = Axle.Axl

local Strb = require "strobe"
local Stobe = Strb.Strobe
local tstrobe = Strb.test

local opt = ffi.new("struct tnt_optd")
local v4l2 = ffi.new("struct v4l2_buffer")
local si = ffi.load("./libsend_image.so")
local tcd = ffi.load("/usr/lib/libtcd.so")

JPG.test()
--Grab.test()
--Axle.test()


-- local TNT_WAIT_NEXT = S.c.IOCTL._IOR('T', 16, ffi.sizeof("struct tnt_wait"))
-- strb:connect("internal", "chb")
-- strb:level("internal", "level_low")
-- strb:time("internal", 1000)



--local axl_sens = Sensor:new("imx265crop", cc.SONY_IMX265_BW_CROPPED_2MP, "/dev/v4l-subdev0")
--local grabmode = ffi.new("enum tnt_grabmode")
--jpeg.test()



-- local bw  = Sensor:new("imx265bw",  cc.CMOS_IMX265_BW, "/dev/v4l-subdev0")
-- local col = Sensor:new("imx265col", cc.CMOS_IMX265_COL, "/dev/v4l-subdev1")
-- local axl_sens = Sensor:new("imx265crop", cc.SONY_IMX265_BW_CROPPED_2MP, "/dev/v4l-subdev0")
-- local alias = { cha="cha", chb="chb" }
-- local grabber = Grabber:new()

-- local sens = axl_sens
-- local init, err = grabber:continuous_chb(sens)
-- if err then
--    print(err)
--    os.exit()
-- end

-- local strb = Strobe:new()
-- strb:connect("internal", "chb")
-- strb:level("internal", "level_low")
-- strb:time("internal", 1000) 

--io.read()
print("-- END --")

