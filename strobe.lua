local ffi = require "ffi"
local S = require "syscall"
local c = S.c
local cc = ffi.C

ffi.cdef[[
struct strobe_data{
    uint32_t channel;
    uint32_t value;
};

enum strobe_connection {
	CONNECT_GND = 0,
	CONNECT_CHA,
	CONNECT_CHB,
	CONNECT_CHAB,
	CONNECT_SW,
};

]]

local STROBE_LEVEL   = S.c.IOCTL._IOWR('T', 24, ffi.sizeof("struct strobe_data"))
local STROBE_CONNECT = S.c.IOCTL._IOWR('T', 25, ffi.sizeof("struct strobe_data"))
local STROBE_TIME    = S.c.IOCTL._IOWR('T', 26, ffi.sizeof("struct strobe_data"))
    


Strobe = {
   path = "/dev/strobe",
   ["internal"] = 8,
   ["external"] = 0,
   ["gnd"] = 0,
   ["cha"] = 1,
   ["chb"] = 2,
   ["level_low"] = 0,
   ["lelvel_high"] = 1
}

function Strobe:new()
   local s = { fd = 0 }
   setmetatable(s, {__index = self})
   return s
end



function Strobe:get_channel(channel)
   if self[channel] == nil then
      print("no channel")
      return nil
   end
   return  self[channel]
end

function Strobe:connect(channel, source)
   local fd,err =  assert(S.open(self.path, "rdwr"))
   if err then print(err) end
   local strb = ffi.new("struct strobe_data")
   strb.channel = self:get_channel(channel)
   local ret = 0
   strb.value = self[source]
   ok, err = fd:ioctl(STROBE_CONNECT, strb)
   if err then print(err) end
   fd:close()
end

function Strobe:time(channel, time)
   local fd,err =  assert(S.open(self.path, "rdwr"))
   if err then print(err) end
   local strb = ffi.new("struct strobe_data")
   strb.channel = self:get_channel(channel)
   local ret = 0
   strb.value = time
   ok, err = fd:ioctl(STROBE_TIME, strb)
   if err then print(err) end
   fd:close()
end


function Strobe:level(channel, level)
   local fd,err =  assert(S.open(self.path, "rdwr"))
   if err then print(err) end
   local strb = ffi.new("struct strobe_data")
   strb.channel = self:get_channel(channel)
   local ret = 0
   strb.value = self[level]
   ok, err = fd:ioctl(STROBE_LEVEL, strb)
   if err then print(err) end
   fd:close()
end


return Strobe
